#RegisterFormVerify
##regCheckOut使用说明：
###1：在项目中引入regCheckOut.js文件
###2：在注册页面的html文件中，对元素绑定事件
```html
<!--例如：-->
<!--用户名：绑定事件：ms-on-keyup="verifyUsername" ms-on-blur="submitUN"-->
 <input type="text" class="form-control"  placeholder="请输入用户名" ms-duplex="regAccount" ms-on-keyup="verifyUsername" ms-on-blur="submitUN">

<!--密码：绑定事件：ms-on-keyup="verifyPassword"-->
 <input type="password" class="form-control"  placeholder="请输入密码" ms-duplex="regPWD" ms-on-keyup="verifyPassword">

 <!--邮箱：绑定事件：ms-on-keyup="verifyEmail" ms-on-blur="submitEM"-->
 <input type="email" class="form-control"  placeholder="请输入电子邮箱用以找回密码" ms-duplex="regMail" ms-on-keyup="verifyEmail" ms-on-blur="submitEM">

<!--注册按钮：绑定事件：ms-click="reg"-->
<button type="button" class="btn btn-primary" ms-click="reg">注册</button>
```
###3：在注册页面的js文件（逻辑层）中，触发插件中的验证事件
```javascript
//例如：
 //注册
        regAccount: '',   //账户
        regPWD: '',   //密码
        regMail: '',   //邮箱
        //验证用户名
        verifyUsername: function(){
            regCheckOut.verifyUsername(reg.regAccount);
        },
        //验证用户名是否被使用
        submitUN: function(){
            regCheckOut.submitUN(reg.regAccount);
        },
        //验证密码
        verifyPassword: function(){
            regCheckOut.verifyPassword(reg.regPWD);
        },
        //验证邮箱
        verifyEmail: function(){
            regCheckOut.verifyEmail(reg.regMail);
        },
        //验证邮箱是否被使用
        submitEM: function(){
            regCheckOut.submitEM(reg.regMail);
        },
```
###4：点击注册按钮后，发出注册请求前，需触发regCheckOut.register(un,pw,em)事件，根据返回值判断用户名、密码、邮箱的输入是否都正确，如果返回1，则表示输入都正确，才能发出注册请求，如果返回0，则不能发出注册请求。
```javascript
reg: function () {
    var isPass = regCheckOut.register(reg.regAccount, reg.regPWD, reg.regMail);
        if (isPass == 1) {
            tip.on('注册中。。。。。。', 1);
            //发出请求
            avalon.ajax({
                type: 'post',
                url: 'http://api.tansuyun.cn/index.php?i=8',
                data: {
                username: reg.regAccount,
                pwd: reg.regPWD,
                email: reg.regMail
                },
                success: function (data) {
                    if (data.c == 200) {
                        tip.off('注册中。。。。。。', 1);
                        tip.on("注册成功!!!", 1, 3000);
                        window.location.href = '#!/login';
                        reg.regAccount = '';
                        reg.regPWD = '';
                        reg.regMail = '';
                        reg.regTel = '';
                        reg.regName = '';
                        reg.regUnit = '';
                    }
                    else {
                        tip.off('注册中。。。。。。', 1);
                        tip.on("注册失败!!!", 0, 3000);
                        tip.on(data.m,0,3000);
                        }
                    }
                });
            }
        },
```




